package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {
	
	
	@Test
	public void ConvertFromCelsiusTest() {
		int result = Fahrenheit.convertFromCelsius(31);
		System.out.println("Regular:  " + result);
		assertTrue("The conversion is incorrect", result == 88);
	}
	
	@Test
	public void ConvertFromCelsiusBoundaryIn() {
		int result = Fahrenheit.convertFromCelsius(1);
		System.out.println("Boundary In:  " + result);
		assertTrue("The conversion is incorrect", result == 34);
	}
	@Test
	public void ConvertFromCelsiusBoundaryOut() {
		int result = Fahrenheit.convertFromCelsius(3);
		System.out.println("Boundary Out:  " + result);
		assertTrue("The conversion is incorrect", result == 38);
	}
	@Test (expected = AssertionError.class)
	public void ConvertFromCelsiusException() {
		int result = Fahrenheit.convertFromCelsius((int)2.5);
		System.out.println("Exception:  " + result);
		assertTrue("The conversion is incorrect", result == 31);
	}
}
